export default {
  "historical-context":{
    "early-life":{
      "header":{
        "text":"Early Life",
        "img":"https://therunnereclectic.files.wordpress.com/2014/07/louis_z.jpg",
        "caption":""
      },
      "html":
`
[tab]The “Unbroken” hero, Louis Zamperini had a very interesting childhood. On January 26, 1917 in Olean, New York, little Zamperini was born. Even though Louis Zamperini was born in New York, he grew up in Torrance, California. During his childhood, he didn’t speak English due to his Italian parents, Anthony and Louise Zamperini. Zamperini grew up with three siblings (two sisters and one brother). His father worked as a construction worker, coal miner, a boxer, and to teach Zamperini to fight against bullies, his father taught him to box. Zamperini’s mother Louise had been married at 16, and had birthed Zamperini when she was 18. His older brother’s name was Pete and his two sisters names were, Sylvia and Virginia. Growing up, Louis Zamperini was quite the troublemaker. His parents were worried he’d be sent to jail because of him throwing tomatoes at a cop, popping his teacher’s car tires, and hopping trains in Mexico. Later in life, when Zamperini was in high school, he became a part of the track team. While he was a part of the track team, his brother Pete in 1934 influenced Louis Zamperini to set a high school track record that wasn’t beaten for 20 years!
`
    },
    "running-and-1936-olympics":{
      "header":{
        "text":"Running and 1936 Olympics",
        "img":"http://www.canyon-news.com/wp-content/uploads/2015/08/louis.jpg"
      },
      "html":
`

`
    },
    "wwii":{
      "header":{
        "text":"WWII",
        "img":"https://resize.rbl.ms/simage/https%3A%2F%2Fassets.rbl.ms%2F17284127%2Forigin.jpg/1200%2C718/k%2B2Ykgq4lWRX8l6M/img.jpg"
      },
      "html":
`

`
    }
  },
  "events":{
    "plane-crash-into-pacific-ocean":{
      "header":{
        "text":"Plane Crash into Pacific Ocean",
        "img":"http://www.b24bestweb.com/images/B24/GREENHORNET-V1.JPG"
      },
      "html":
`
[tab]On May 17, 1943, Zamperini went on a mission to find a missing pilot on the “Green Hornet” with another plane, the “Daisy Mae.” The “Green Hornet” was unable to keep up and the other plane went ahead.  Once they reached the searching area, they declined to 800ft to be able to see.  Then, one of the left engines died.  While the engineer was trying to “feather” the engines to keep the plane balanced, he accidentally broke the other left engine.  They prepared to crash by moving to the tail of the plane.  Then, the plane crashed into the ocean about 800 miles away from Hawaii.  Zamperini broke his ribs by slamming into the gun mount during the crash, and also got caught in the wires for the gun.  He managed to escape the wires while he was unconscious, and pulled himself out because his ring was caught on the frame of the opening for the gun.  After this, his life vest pulled him up to the surface.  Eight out of the eleven on the “Green Hornet” died, and him, the pilot Russell (Phil) Allen Phillips (whose head was bleeding), and the tail gunner Francis (Mac) McNamara were the only survivors of the crash.
`
    },
    "surviving-on-raft":{
      "header":{
        "text":"Surviving on Raft",
        "img":"https://postperspective.com/wp-content/uploads/2014/12/Cropped-Raft-2.jpg"
      },
      "html":
`
[tab]After the crash, they found two rafts from the plane, which were about six by two feet, and Zamperini and Mac went in one and Phil went into the other.  They used their shirts to bandage Phil’s head, and Phil gave the command to Zamperini because he was injured.  Since their provisions box was gone, all they had with them was what was in the raft pocket.  The pocket contained chocolate bars, half-pint water bottles, a brass mirror, a flare gun, sea dye, fishing hooks, fishing line, a leak patch kit, air pumps, pliers, and a screwdriver.  Zamperini made a system for provisioning the food, which was one square of chocolate every morning and night, and two to three sips of water a day.  However, Mac ate all of the chocolate while the others were asleep, so they were left without any other food.  Every day was very hot and the night was very cold.  Once the “Daisy Mae” landed, a search for them began.  In the first few days, two friendly planes flew over, but neither noticed the flares or the sea dye.  They started to realize that the raft was moving west, which was away from the American planes.  On the fourth day, their water ran out.  Mac became frustrated and thought that they would not survive.  On the seventh day on the raft, they were officially declared missing.  Even so, Zamperini’s mother still believed that Zamperini was still alive.  When the rain came on the eighth day, they collected it in the air pump cases.  On the ninth or tenth day, an albatross landed on Zamperini’s air pump case hat, however, after killing it, none of them could eat it because of the smell, and they decided to use it as fishing bait.  When they tried to fish, the first two fishing lines got caught by the sharks that were following their boat, but the third one caught a fish finally.  Zamperini knew he had to keep their brains active to prevent them from going crazy from Dr. Eugene Roberts at USC.  So, he was constantly asking them questions, they were talking about their lives, and they sang “White Christmas.”  Because they had nothing to eat, they used describing their favorite foods as a substitute.  By the second week, the yellow of the raft started to get on their skin and they started to get salt sores.  Also, instead of praying to himself, Zamperini started praying out loud.  Around then, another albatross landed on Zamperini, and this one was edible, and they used the scraps for fishing bait.  When they ran out of this bait, they were able to catch pilot fish by tying the fishing hooks to their fingers.  Also, Phil caught a tern which Zamperini ate, but he found that it had lice and he had to wash them off in the ocean.  After a prayer by Zamperini for rain, the rain came after many days without water.  Later, they saw a plane and shot a flare, but it was actually a Japanese plane.  The plane started shooting them, and they jumped into the water.  Then, a shark tried to eat Zamperini’s leg but backed off after Zamperini punched its nose.  The plane dropped a depth charge, but it luckily didn’t explode.  Phil’s raft was completely destroyed by the gunfire, so everyone moved to the other raft, which still had bullet holes.  They started a routine of patching the bullet holes in the day and inflating the raft at night.  They calculated that they were about halfway to the Marshall Islands and the Gilbert Islands.  During this time, Mac wasn’t talking at all because he had lost hope.  Once during the night, a shark tried to eat Zamperini’s head, and he hit it in the nose.  Then, another shark jumped up, and Mac hit it with an oar, which saved Zamperini’s life.  Later, Phil and Zamperini were able to catch two sharks and eat their livers, because they knew that that was the only part that was edible.  At about the thirtieth day, a shark hit the bottom of their raft and sent it flying into the air.  On the thirty-third day, Mac died and they buried him at sea.  The next day, they broke the record for the longest time on a raft.  On the forty-sixth day, they finally could see an island in the distance.  However, they were taken into a typhoon, where there were high waves, rain, and wind, and their raft almost flipped over.  The next day, they were captured by a Japanese motorboat by the Marshall Islands. This ended their forty-seven days on the raft, where they traveled about 2000 miles, and Zamperini lost 80 of the 160 pounds that he weighed before.

`
    },
    "captivity-and-torture-by-japanese-army":{
      "header":{
        "text":"Captivity and Torture by Japanese Army",
        "img":"https://www.awesomestories.com/images/user/d465e2d7704fc5fbad02e0deaab15230.jpg"
      },
      "html":
`
[tab]At first, after he was captured, the Japanese pretended to be nice to him.  He was transferred to a boat and given biscuits and coconut.  Then he was taken to an infirmary and was given even more food and told his story to the officers.  But afterward, he and Phil were sent to a camp called Kwajalein, where he was locked in a cell with dirt floors and bugs.  It was nicknamed “Execution Island”, and one of the guards said that the last prisoners were decapitated and that they would be killed too.  All he got was rice balls and small cups of tea.  The guards threw rocks and cigarettes at him and made him dance and laughed at him.  When submarines passed by, the crews would line up to abuse the prisoners.  He started to lose his self-respect and will to live.  Then, he was interrogated about the plane equipment.  Then, he became friends with a Christian guard named Kawamura, who stood up for Zamperini by beating up a guard that attacked Zamperini.  Also at Kwajalein, he and Phil were experimented on by injecting them for times, which made them lightheaded, have rashes, and itch.  Later, he was interrogated again and gave fake base locations.  They were both not killed, and on August 26, 1943, Zamperini was sent to Ofuna interrogation camp after forty-two days at Kwajalein.  On the ship there, his nose was broken by drunk sailors.  Zamperini thought that Ofuna would be a POW camp protected by the Geneva Conventions, but it was not.  There were rules for everything, such as not being able to speak to anyone except the guards and always having to look down.  The guards were allowed to do anything they wanted to the captives, and the captives were beaten by them.  The guards hated the captives because, in the Japanese Army, the soldiers should always die instead of being captured, and because they were taught to be racist towards them.  All of the prisoners were very thin, and they got diseases and parasites from the food.  However, the captives tried to get around the rules.  They would communicate with morse code, and they would talk by crowding around the guards and pretending that they were asking questions.  Also, Zamperini had a secret diary.  Zamperini became the thinnest person at the camp.  After being at the camp for a certain amount of time, he was allowed to talk to other people.  They would learn about the news of the war by asking new people and stealing newspapers.  He became friends with Lt. William Harris and Frank Tinker.  Together, they made a plan to escape, however they canceled it after they learned that all people that escape will be killed.  Later, Zamperini stole a war map from a guard called “the Quack.”  Harris drew the map and then it was put back.  But, “the Quack” found out and attacked Harris with a crutch, and even continued hitting Harris after he was unconscious.  The attack left Harris with brain damage, and he forgot who his friends were.  On September 31, 1944, he was transferred to Omori POW camp, which he hoped would be better, but it was not.  At Omori, there was the most brutal guard that he had, Corporal (later Sergeant) Mutsuhiro Watanabe, who was called “the Bird.”<div class="wrap-img-wrapper right-align"><img src="https://reasonabletheology.org/wp-content/uploads/Mutsuhiro_Watanabe.jpg" class="wrap-img"><span class="wrap-img-caption">Mutsuhiro Watanabe</span></div>  As soon as he came to Omori, “the Bird” was already after him.  When he got to Omori, just because Zamperini didn’t look at “the Bird's” face when saying Zamperini’s own name, and hit him again when he made a fire to warm the prisoners.  “The Bird” was always angry because he came from a family of good soldiers, but he was not.  He specifically hated Zamperini because he was famous and didn't follow orders, and called him his “number one prisoner.”  “The Bird” was known for his use of psychological torture.  As an example, he burned people’s family photos in front of them, and he made someone salute a flagpole during a whole night.  When the officers told “the Bird” that Omori was a slave labor camp and didn't follow the Geneva Conventions, he hit them and made them clean the bathrooms that had diarrhea in them.  Also, all of the captives were registered for the Red Cross except Zamperini.  Even so, all of the Red Cross packages were stolen by the Japanese.  To keep up their spirits, the prisoners tried to go against the Japanese by switching mail addresses for goods, sinking barges, breaking train tracks, and stealing items such as sugar.  Captives with lots of sugar gave to people that were starving, which helped more people stay alive.  On November 1, 1944, the first B-29 flew over Tokyo from Saipan and sirens rang, which made the prisoners happy.  After the plane flew over, “the Bird” was angry and hit Zamperini on the head with a belt buckle, which made his head bleed, and hit him again after pretending to be kind.  Later, Radio Tokyo let him send a message to the U.S. that he was alive on a Japanese propaganda show called Postman Calls.  His family finally knew that he was alive after a year because of the message.  Then, Radio Tokyo tried to take advantage of him by telling him to send a propaganda message to the U.S., which he refused to.  Then, he realized that why he was not killed was because he could be used for propaganda because he was famous.  On November 24, 1944, 111 B-29s flew over Omori and Tokyo.  “The Bird” became mad that the Americans were winning and would hurt more people and run around with a sword when there were bombings.  Also, “the Bird” would attack Zamperini every day until he was bleeding, which gave him dreams of “the BIrd” attacking him and then him choking “the Bird.”  Finally, “the Bird” was moved to another camp when he was promoted sergeant and the new Japanese let them send letters and get Red Cross packages.  From February 16, 1945, Tokyo was attacked again, with many planes for days.  On the seventh day, 229 B-29s firebombed Tokyo, which set the city on fire and destroyed a lot.  On February 28, 1945, Zamperini was moved to another camp called Naoetsu which was cold and snowy, and unfortunately, “the Bird” was there again.  At first, Zamperini was not given work because he was an officer, but later he had to carry fertilizer and got a full ration because of that.  But, a guard that went with them made a joke to “the Bird” that they were lazy, and “the Bird” made the officers carry coal.  Later, Zamperini was changed to carrying salt, and a guard purposely knocked over Zamperini, which prevented him from walking, so he had to stay at the camp with half a ration.  When he asked for a full ration, “the Bird” made him clean a pig sty with his bare hands.  When Zamperini was able to walk again, there was a report that prisoners stole fish, and he and five other officers were chosen by “the Bird” for punishment.  He forced the other captives to punch the officers, and they were punched about 220 times.  In the end, their faces were swollen.  As more people came to the camp, the rations got less, and there was a plan for all captives to be killed on August 22, 1945.  Later, “the Bird” made Zamperini hold a beam, and after 37 minutes of holding the beam, “the Bird” got mad and punched him, which made the beam fall on Zamperini’s head and knock him out.  When the largest air raid flew over, “the Bird” hit everyone with a kendo stick.  Later, “the Bird” said that he was going to drown Zamperini.  Later, the officers made a plan to kill “the Bird” by attaching him to a rock and dropping from the top of the barracks into the river.  On August 6, 1945, an atomic bomb was dropped on Hiroshima, and the captives learned about it soon after.  Then, the second one was dropped on Nagasaki on August 9.  On August 11, “the Bird” left to prepare the killing camp for August 22.  When a guard said that the war was over, no one believed him.  But, once they were given no work, then they knew that the was over.  By this time, Zamperini was very thin and sick.
`
    },
    "liberation-from-camp":{
      "header":{
        "text":"Liberation from Camp",
        "img":""
      },
      "html":
`
[tab]On August 20, 1945, the Japanese confirmed that the war was over, and let the prisoners bathe in the river.  Then, an American plane flew over which also told them the war was over and gave them a bar of chocolate, which was spilled into 700 pieces for every person to get one.  Then, another plane came which dropped candy bars, cigarettes, and magazines showing the atomic bomb.  On August 26, American fighter planes flew over, and the POWs wrote “food” and “smokes” on the ground.  However, the planes didn’t have anything and instead did an airshow, which convinced the Japanese to bring food trucks.  On August 28, 1945, 6 B-29s flew over and gave lots of food.  They got a message that they were going to be liberated on September 4, 1945, but they were not.  So, Commander Fitzgerald went to the train station and tells them to bring a train for them the next day.  When the person refused, Fitzgerald punched him, and the train came the next day.  While they were on the train, they saw how so much of Japan was destroyed.  The train stopped in Yokohama, and he took multiple flights back to the U.S., and on the way, he saw how Kwajalein was destroyed.  Zamperini was taken to Letterman General Hospital in San Francisco.  Pete came to see Zamperini and Pete had lost weight and was bald while Zamperini was gone.  Once he was done there, he flew to Long Beach Airport and was reunited with the rest of his family.
`
    }
  },
  "impact-legacy-relation-to-today":{
    "forgiving-captors":{
      "header":{
        "text":"Forgiving Captors",
        "img":"https://static.billygraham.org/sites/billygraham.org/uploads/pro/2014/12/29142855/zampfeature.jpg"
      },
      "html":
`
[tab]When Zamperini returned, he was a war hero.  However, he would always have nightmares of “the Bird” and wanted to kill “the Bird.”  He turned to alcohol, and his wife, Cynthia Applewhite,  was going to divorce him.  He overcame these problems by becoming a stronger Christian.  After his wife went to a Billy Graham sermon, she convinced him to go because she wasn’t going to divorce him anymore.  After leaving the first one, he finally convinced on the second sermon to become Christian, because he remembered praying in the raft and the camps to serve God when he came back.  After this, he never had these nightmares again and did not want revenge anymore.  Later, he would be friends with Graham.  In 1952, Zamperini went to Sugamo Prison in Tokyo which had 850 war criminals.  There, he found some of his captors and individually forgave them, and all of them except one became Christian.  In 1998, he came to Japan to carry the flame for the Olympics there, and to forgive “the Bird.”  However, “the Bird” refused to meet him, and Zamperini wrote a letter to him instead.
`
    },
    "books-and-movies":{
      "header":{
        "text":"Books and Movies",
        "img":"https://images-na.ssl-images-amazon.com/images/I/91UmKFGfyYL.jpg"
      },
      "html":
`
[tab]Multiple books and movies were based on Zamperini’s story.  The first is Devil at My Heels: the Story of Louis Zamperini, which was written by Zamperini and Helen Itris in 1956.  In 2003, Zamperini created a book with the same title, Devil at My Heels: A Heroic Olympian's Astonishing Story of Survival as a Japanese POW in World War II, with David Resin.  Then, the most well-known book about him now, Unbroken: A World War II Story of Survival, Resilience, and Redemption, was written in 2010 by Laura Hillenbrand, and became a #1 New York Times bestseller.  On December 25, 2014, the first movie about Zamperini was released and was directed by Angelina Jolie, but did not show his forgiveness.  On September 14, 2018, Unbroken: Path to Redemption, which was directed by Harold Cronk, was released, and showed his forgiving of his captors.
`
    },
    "the-war-in-syria":{
      "header":{
        "text":"The War in Syria",
        "img":""
      },
      "html":
`
Zamperini’s story relates to the present day U.S backed war in Syria because the civilians there have gone through many hardships throughout the war, but now they are starting to recover and rebuild.  The conflict started in 2011, as well as the U.S. involvement.  The U.S. was arming rebels fighting against the Syrian President Bashar al-Assad and the Syrian government.  Russia and Iran support Assad.  In 2012, the rebels took over eastern Aleppo, and in the summer of that year, the C.I.A. director then, David H. Petraeus made another plan to arm rebels fighting against the Syrian government, which was refused by the president.  But, in 2013, he accepted a plan for the C.I.A to secretly arm and train rebels for this purpose in bases in Jordan.  In September  2014, the Senate voted 78-22 for an amendment to the spending bill to arm rebels that were supposed to fight against ISIS.  However, at that time, they were fighting against the Syrian government.  This conflict had a great impact on the civilians of Syria.  In September 2016 Aleppo, the largest city in Syria, 2 million of the population didn’t have running water for two years, and the city is sometimes cut off from supplies.  Western Aleppo was controlled by the government, and eastern Aleppo was controlled by rebels, and the front lines were in the Old City in Aleppo, which is a Unesco world heritage site.  The rebels destroyed churches and wanted to remove the Christians.  An example of the rebuilding is that both Christians and Muslims raised money to build a new Protestant church in Aleppo because the old one was destroyed.  The churches in Aleppo give water, food, and medical care to both Christians and Muslims.
`
    }
  }
}
