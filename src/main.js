import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import MuseUI from 'muse-ui';
import 'muse-ui/dist/muse-ui.css';
import VueMq from 'vue-mq'
import VueMarkdown from 'vue-markdown'
import VueYouTubeEmbed from 'vue-youtube-embed'
import LoadScript from 'vue-plugin-load-script';
import VueVideoPlayer from 'vue-video-player'
import 'videojs-hotkeys';
// require videojs style
import 'video.js/dist/video-js.css'
// import 'vue-video-player/src/custom-theme.css'

Vue.use(VueVideoPlayer, /* {
  options: global default options,
  events: global videojs events
} */)

Vue.use(LoadScript);

Vue.use(VueYouTubeEmbed)
Vue.use(MuseUI);
Vue.use(VueMq, {
  breakpoints: {
    mobile: 1100,
    tablet: 1200,
    laptop: 1250,
    desktop: Infinity,
  }
})
Vue.component('vue-markdown',VueMarkdown);

Vue.config.productionTip = false

window.vm = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
