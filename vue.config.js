var path = require('path')
module.exports = {
  baseUrl: process.env.NODE_ENV === 'production' ? '/nhd/' : '/',
  configureWebpack: config =>{
    config.optimization = {
      minimize: false
    }
  },
  devServer: {
      disableHostCheck: true
  }
}
