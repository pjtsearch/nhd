# Louis Zamperini NHD
#### by Giovanni and Peter

Louis Zamperini NHD is an National History Day project website.  The theme of the project is "triumph and tradgedy."  During World War II Louis Zamperini survived an incredible forty-seven days on a raft in the middle of the ocean with barely any supplies and then was captured by the Japanese and tortured. By having a very strong mindset and immense courage, Louis Zamperini triumphed over death and many other terrible experiences. His story showed many other people that a person can survive what seems impossible if he or she is mentally strong, and has an undefeatable will to live.

## Hosted at:
[https://nhd.pjtsearch.com](https://pjtsearch.com/nhd)

[https://pjtsearch.gitlab.io/nhd/](https://pjtsearch.gitlab.io/nhd/)
## Getting project

### Prerequisites
Node

Yarn or NPM

### Clone repository
```
git clone https://gitlab.com/pjtsearch/nhd.git
```

### Install modules
```
yarn #or npm install
```

## Scripts

### Compiles and hot-reloads for development
```
yarn serve #or npm run serve
```

### Compiles and minifies for production
```
yarn build #or npm run build
```
